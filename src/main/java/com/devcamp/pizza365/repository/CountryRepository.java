package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.model.CCountry;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);

	@Query("SELECT c FROM CCountry as c WHERE c.countryName LIKE %?1%")
	List<CCountry> findByNameLike(String name, Pageable pageable);

	@Query("SELECT c FROM CCountry as c WHERE c.countryName LIKE :name%")
	List<CCountry> findByNameLikeRight(@Param("name") String name, Pageable pageable);

	@Query(value = "SELECT * FROM p_country as c WHERE c.country_name LIKE %:name", nativeQuery = true)
	List<CCountry> findByNameLikeLeft(@Param("name") String name, Pageable pageable);
}
